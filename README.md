
# Styled Components

1.**Styled-components располагаем под объявлением компонента, где они используются**

<details>
  <summary>Пример</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    function SimpleComponent() {
      return (
        <Root>Hello World!</Root>
      );
    };

    const Root = styled.section`
      margin: 0 0 10px;
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

2.**Стили пишем в css синтаксисе, никаких объектов со стилями**

<details>
  <summary>Плохо</summary>

  ```javascript
    const Root = styled.div({
      background: 'white',
    });
  ```

</details>

<details>
  <summary>Хорошо</summary>

  ```javascript
    const Root = styled.div`
      background: white;
    `
  ```

</details>

<br/>

3.**Названия для пропсов стилевого компонента начинаем со знака $. <br/>Это относится к пропсам, которые используются только для стилей и не должны попадать в элемент, который стилизуется**

<details>
  <summary>Пример</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    function SimpleComponent({ isDisabled }) {
      return (
        <Root $isDisabled={isDisabled}>Hello World!</Root>
      );
    };

    const Root = styled.section`
      margin: ${({ $isDisabled }) => `0 0 ${$isDisabled ? 10 : 5}px`};
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

4.**Если нужно изменить стили в зависимости от переменной, то передаем только переменную, а не те стили, что будут изменены**

<details>
  <summary>Плохо</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    function SimpleComponent({ color, fontSize }) {
      return (
        <Root $color={color} $fontSize={fontSize}>Hello World!</Root>
      );
    };

    const Root = styled.section`
      margin: 0 0 10px;
      ${({ $color }) => $color && `color: ${$color};`}
      ${({ $fontSize }) => $fontSize && `font-size: ${$fontSize}px;`}
    `;

    export default SimpleComponent;
  ```

</details>

<details>
  <summary>Хорошо</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    function SimpleComponent({ hasError }) {
      return (
        <Root $hasError>Hello World!</Root>
      );
    };
s
    const Root = styled.section`
      margin: 0 0 10px;
      ${({ $hasError }) => $hasError &&
      `
        color: red;
        font-size: 12px;
      `}
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

5.**Если нужно сделать несколько вариантов компонента, то создаем объект с модификаторами (или темами), в котором ключи будут названиями модификаторов, а значения - функциями, возвращающими css**

<details>
  <summary>Пример</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    const themes = {
      primary: () => `
        color: black;
        font-size: 10px;
      `,
      secondary: (isDisabled) => `
        color: ${isDisabled ? 'gray' : 'red'};
        font-size: 12px;
      `,
    }

    function SimpleComponent({ theme, isDisabled }) {
      const themeCSS = themes[theme](isDisabled);

      return (
        <Root $themeCSS={themeCSS}>Hello World!</Root>
      );
    };

    const Root = styled.section`
      margin: 0 0 10px;
      ${({ $themeCSS }) => $themeCSS}
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

6.**Если нужно передать дополнительный css, то передаем его в пропсе $CSS в стилевой компонент**

<details>
  <summary>Пример</summary>

  ```javascript
    import React from 'react';
    import styled from 'styled-components';

    function SimpleComponent({ RootCSS }) {
      return (
        <Root $CSS={RootCSS}>Hello World!</Root>
      );
    };

    const Root = styled.section`
      margin: 0 0 10px;
      ${({ $CSS }) => $CSS}
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

7.**Слишком большую логику по применению css-свойства можно вынести в отдельную переменную-mixin и там описать с помощью метода css``**

<details>
  <summary>Пример</summary>

  ```javascript
    import React from 'react';
    import styled, { css } from 'styled-components';

    function SimpleComponent({ isDisabled, hasError }) {
      return (
        <Root $isDisabled={isDisabled} $hasError={hasError}>Hello World!</Root>
      );
    };

    const rootFontSize = css`
      font-size: ${({ $isDisabled, $hasError }) => {
        if ($isDisabled) {
          return 10;
        } else if ($hasError) {
          return 12;
        }

        return 14;
      }}px;
    `;

    const Root = styled.section`
      margin: 0 0 10px;
      ${rootFontSize}
    `;

    export default SimpleComponent;
  ```

</details>

<br/>

8.**Для linter и autocomplete устанавливаем подходящий плагин для своего IDE - https://styled-components.com/docs/tooling#syntax-highlighting**

<br/>

9.**Для создания семантичного названия класса нужен следующий импорт - ```import styled from 'styled-components/macro';``` Класс будет выглядеть так - название файла, где вызывается styled + "__" + название styled компонента (App__Wrapper-hash). Работает в проектах созданных с помощью create-react-app или с установкой ```babel-plugin-styled-components```**

<br/>

10.**Название корневого элемента в компоненте - Root. Название кастомных стилей: название компонента + CSS (RootCSS, WrapperCSS)**